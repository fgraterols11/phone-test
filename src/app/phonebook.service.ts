import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const regionUrl = "http://private-anon-f565978ffc-testphonebook.apiary-mock.com/region";
const personasUrl = "https://private-anon-f565978ffc-testphonebook.apiary-mock.com/persona";
@Injectable({
  providedIn: 'root'
})
export class PhonebookService {
  

  constructor(private http: HttpClient) { }

  getPersonas(): Observable<any> {
    return this.http.get(personasUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getRegiones(): Observable<any> {
    return this.http.get(regionUrl, httpOptions, ).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
  
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
