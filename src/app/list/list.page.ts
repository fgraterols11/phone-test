import { Component, OnInit } from '@angular/core';

import { LoadingController } from '@ionic/angular';
import { PhonebookService } from '../phonebook.service';
import { load } from '@angular/core/src/render3';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  _listadoPersonas: any[];
  _listadoView: any[]

  _listadoRegiones: any [];

  _regionAux: any;

  _nameFilter = '';
  _regionFilter = 0;
  constructor(public phonebook: PhonebookService, public loadingController: LoadingController) {
  }

  ngOnInit() {
    this.getRegiones();
    this.getPersonas();
  }

  async getRegiones() {
    await this.phonebook.getRegiones()
      .subscribe(res => {
        this._listadoRegiones = res;
      }, err => {
        console.log(err);
      });
  }

  async getPersonas() {
    const loading = await this.loadingController.create({
     //content: 'Loading'
    });
    await loading.present();
    await this.phonebook.getPersonas()
      .subscribe(res => {
        this._listadoPersonas = res;
        this.validarPersonas();
        this._listadoView = this._listadoPersonas;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  async validarPersonas() {
    await this._listadoPersonas.forEach(persona => {
      persona.direccion.region = this.obtenerRegionByComuna(persona.direccion.comuna.id);
      if (this.validarRut(persona.rut)){
        persona.validacionRutIcon = 'checkmark-circle-outline';
        persona.validacionRutColor = 'success';
      } else {
        persona.validacionRutIcon = 'close-circle-outline';
        persona.validacionRutColor = 'danger';

      }

      if (this.validarTelefono(persona.telefono)){
        persona.validacionTelfIcon = 'checkmark-circle-outline';
        persona.validacionTelfColor = 'success';

      } else {
        persona.validacionTelfIcon = 'close-circle-outline';
        persona.validacionTelfColor = 'danger';
      }
    });
  }

  obtenerRegionByComuna(comunaID){
    this._listadoRegiones.forEach(element => {
      element.comunas.forEach(comunaAux => {
        if (comunaAux.id === comunaID) {
          this._regionAux = element;
        }
      });
    });
    return this._regionAux;
  }

  validarTelefono(number) {
    if (number.length === 11) {
      return true;
    } else {
      return false;
    }
  }

  validarRut(rut){
    // Despejar Puntos
    let valor = rut.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    let cuerpo = valor.slice(0,-1);
    let dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    rut = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { return false;}
    
    // Calcular Dígito Verificador
    let suma = 0;
    let multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(let i=1; i <= cuerpo.length; i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        let index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    let dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11: dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if (dvEsperado != dv) {  return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    return true;
  }

  filterByName(event) {
    let listado = this._listadoPersonas;
    this._listadoView = listado.filter(item => {
      let nombre = item.nombre + ' ' + item.apellido;
      return nombre.toLowerCase().indexOf(event.detail.value.toLowerCase()) > -1;
    });
  }

  filterByRegion (event) {
    let listado = this._listadoPersonas;
    this._listadoView = listado.filter(item => {
      console.log(item.direccion.region.id);
      return item.direccion.region.id === parseInt(event.detail.value, 10);
    });
  }

}
""